FROM python:3.6-slim

ARG VERSION=0.0.0

COPY . /build
RUN cd /build && python setup.py sdist
RUN cd /build && easy_install dist/akanoabot-${VERSION}.tar.gz
RUN mkdir -p /code
RUN mv /build/config/config.example.ini /code/config.ini
RUN mv /build/config/logger.ini /code/logger.ini
RUN rm -fr /build

WORKDIR /code

CMD akanoabot --config /code/config.ini --logging /code/logger.ini
