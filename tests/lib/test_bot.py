#!/usr/bin/env python3
# coding: utf-8
'''
    Project     : akanoabot
    File        : TestConfig
    Author      : Noa
    Date        : 22/11/2017
    Description :
'''

import unittest

from akanoabot.lib.bot import Bot


class TestBot(unittest.TestCase):

    def setUp(self):
        self.instance = Bot()


if __name__ == '__main__':
    unittest.main()
