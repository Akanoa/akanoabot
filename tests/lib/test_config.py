#!/usr/bin/env python3
# coding: utf-8
'''
    Project     : akanoabot
    File        : TestConfig
    Author      : Noa
    Date        : 22/11/2017
    Description : 
'''
import os

from akanoabot.lib.config import Config
from unittest.mock import patch
from pyfakefs.fake_filesystem_unittest import Patcher
import unittest


class TestConfig(unittest.TestCase):

    def setUp(self):
        self.instance = Config('/config.ini')

    def testGetValue(self):

        data='''
        [auth]
        username=akanoabot
        '''

        with Patcher() as patcher:
            patcher.fs.CreateFile('/config.ini', contents=data)

            # Get existing value from file
            value = Config.getValue('auth', 'username')
            self.assertEqual('akanoabot', value)

            # Placeholder a non existing field with a default value
            value = Config.getValue('auth', 'username2', default="akanoabot2")
            self.assertEqual('akanoabot2', value)

            #prepare env
            mocked_environement = patch.dict(os.environ, {"AUTH_USERNAME" : "akanoa"})
            mocked_environement.start()

            # Overwrite file value with environment
            value = Config.getValue('auth', 'username', env="AUTH_USERNAME")
            self.assertEqual('akanoa', value)

            # Overwrite file value with environment non existing env variable fallback to file value
            value = Config.getValue('auth', 'username', env="AUTH_USERNAME2", default="username")
            self.assertEqual('akanoabot', value)

            # Overwrite file value with environment non existing env variable fallback to default value
            value = Config.getValue('auth', 'username2', env="AUTH_USERNAME2", default="username")
            self.assertEqual('username', value)

            # destroy env
            mocked_environement.stop()

    def testGetConfig(self):

        data='''
        [auth]
        username=akanoabot
        '''

        with Patcher() as patcher:
            patcher.fs.CreateFile('/config.ini', contents=data)

            config = Config.getConfig()
            self.assertEqual('akanoabot', config.get('auth').get('username'))


if __name__ == '__main__':
    unittest.main()
