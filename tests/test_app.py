#!/usr/bin/python
# -*- coding: UTF-8 -*-

'''
    Project     : akanoabot
    File        : test_app
    Author      : yguern
    Date        : 11/23/2017
    Description :  
'''
import argparse
import os
import unittest

import sys
from unittest import mock

from pyfakefs.fake_filesystem_unittest import Patcher

from akanoabot.app import main


loggerFilePath = os.path.abspath("./config/logger.ini")

class MyTestCase(unittest.TestCase):

    def setUp(self):
        pass

    @mock.patch('argparse.ArgumentParser.parse_args',
                return_value=argparse.Namespace(config="/config.ini", logging=loggerFilePath))
    def test_main(self, mock_args):



        data='''
        [auth]
        username=akanoabot
        '''

        with open(loggerFilePath) as f:
            loggerData = f.read()

        with Patcher() as patcher:
            patcher.fs.CreateFile('/config.ini', contents=data)
            patcher.fs.CreateFile(loggerFilePath, contents=loggerData)

            with self.assertRaises(SystemExit) as context:
                main()
            self.assertEqual('0', str(context.exception))


    @mock.patch('argparse.ArgumentParser.parse_args',
                return_value=argparse.Namespace(config="/config.ini", logging=loggerFilePath))
    def test_failure_main_non_existing_configuration(self, mock_args):

        with open(loggerFilePath) as f:
            loggerData = f.read()

        with Patcher() as patcher:
            patcher.fs.CreateFile(loggerFilePath, contents=loggerData)

            with self.assertRaises(Exception) as context:
                main()
            self.assertEqual("Please specifiy an existing configuration file (--config /directory/file.ini)", str(context.exception))

    @mock.patch('argparse.ArgumentParser.parse_args',
                return_value=argparse.Namespace(config="/config.ini", logging=loggerFilePath))
    def test_failure_main_on_non_existing_logging(self, mock_args):

        data='''
        [auth]
        username=akanoabot
        '''

        with Patcher() as patcher:
            patcher.fs.CreateFile('/config.ini', contents=data)

            with self.assertRaises(Exception) as context:
                main()

            self.assertEqual("Please specifiy an existing logging file (--logging /directory/file.ini)", str(context.exception))


if __name__ == '__main__':
    unittest.main()
