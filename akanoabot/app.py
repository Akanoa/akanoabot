#!/usr/bin/env python3
# coding: utf-8
'''
    Project     : akanoabot
    File        : app
    Author      : Noa
    Date        : 22/11/2017
    Description : 
'''
import argparse
from logging.config import fileConfig

from akanoabot.lib.bot import Bot
from akanoabot.lib.config import Config

from os import path

def main():

    parser = argparse.ArgumentParser(description='Start AkanoaBot')
    parser.add_argument('--config', help="Configuration file", default='./config/config.example.ini')
    parser.add_argument('--logging', help="Logger configuration file", default='./config/logger.ini')
    args = parser.parse_args()

    if not path.isfile(args.config):
        raise Exception("Please specifiy an existing configuration file (--config /directory/file.ini)")

    if not path.isfile(args.logging):
        raise Exception("Please specifiy an existing logging file (--logging /directory/file.ini)")

    # Configuring logging properties
    fileConfig(args.logging)
    Config(args.config)

    # Start the Bot
    Bot()

    # Exit with a 0 error code in order to to pass execution test
    exit(0)

if __name__ == '__main__': # pragma: no cover
        main()