#!/usr/bin/env python3
# coding: utf-8
'''
    Project     : akanoabot
    File        : Bot
    Author      : Noa
    Date        : 22/11/2017
    Description : 
'''

import logging

logger = logging.getLogger("akanoabot")

class Bot(object):
    """
    An IRC bot able to speak on twitch
    """
    def __init__(self):
        """
        Bot constructor
        """
        logger.info("Starting AkanoaBot")