#!/usr/bin/env python3
# coding: utf-8
'''
    Project     : akanoabot
    File        : config
    Author      : Noa
    Date        : 22/11/2017
    Description : Parse an ini file extract values from it, if environment variable is provided, file value is overwritten
'''
import os
from configparser import ConfigParser


class Config(object):

    configurationFile = ''
    parser = None
    config = {}

    def __init__(self, configurationFile):
        """
        Construct a new Config hydratation object
        :param configurationFile: The absolute path toward configuration file ( format INI )
        """
        Config.configurationFile = configurationFile
        Config.config = {}

    @staticmethod
    def getValue(section, field, default=None, env=None):
        """
        Get value from INI file, use env value if provided
        :param section: Name of the section aimed
        :param field: Name of the field section aimed
        :param default: Default value used whether field doesn't exist
        :param env: Environment key use to overwritten field value in ININ file
        :return: The field value
        """
        if not Config.parser:
            # Create a INI file parser
            Config.parser = ConfigParser()
            # Read the config file in order to get each values
            Config.parser.read(Config.configurationFile)

        if env and env in os.environ:
            return os.environ[env]

        return Config.parser.get(section, field, fallback=default)

    @staticmethod
    def getConfig():

        if not Config.config:

            # Authentication
            Config.config['auth'] = {}
            Config.config['auth']['username'] = Config.getValue('auth', 'username')



        return Config.config