from setuptools import setup

setup(
    name='akanoabot',
    version='0.0.3',
    packages=['tests', 'tests.lib', 'akanoabot', 'akanoabot.lib'],
    url='',
    license='MIT',
    author='Noa',
    author_email='dev@guern.eu',
    description='A twitch bot',
    entry_points={
        'console_scripts': ['akanoabot=akanoabot.app:main'],
    },
    install_requires=[
        'requests',
        'pyfakefs',
        'pytest-cov'
    ]
)
